Importacion fotos y datos de una base de datos 

Este progama tiene dos funciones:
1º. Pasa un archivo JSON a uno excel con un fomato determinado, se ha probado la ejecución de ésta funcionalidad con los archivos de "efiluxdb" y "elementos_efilux_mobile"
2º. Si el programa encuentra un documento adjunto al archivo JSON lo saca y lo guarda en un archivo de tipo imagen(se han probado los archivos "jpeg" y "jpg")

Para ejecutar este programa se necesitan tener instaladas en nuestro entorno virtual:
    - json //para leer bien los json que se reciben 
    - pandas //para facilitar la transformacion de json a excel 
    - request //para hacer el request de la base de datos
    - openpyxl //para volcar los datos a un excel 
    - sys //para leer argumentos pasados desde la terminal

Para ejecutar (cambiar la URL por la que proceda):
    -python3 obtenerjson.py 'primera_parte_url' 'parte variable url' 
Ejemplo de ejecución:
    -python obtenerjson.py 'https://gesletter.es:5995//efiluxdb/' '_all_docs'


